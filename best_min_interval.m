function best_pos = best_min_interval(value, column, base_value, base_column, centroids, interval)
    
    % value  = uma das coordenadas recebida com o click
    % column = coluna em que vamos procurar o valor mais próximo de value
    
    % base_value  = valor da segunda coordenada recebida com o click
    % base_column = coluna do valor base_value
  
    % centroids = array de centroids
      
    % interval = intervalo da busca
    
    % Funcionamento:
    % X e Y são recebidos como entrada (click), a função adiciona um intervalo para
    % cima e para baixo de X e na array de centroids e busca o melhor valor
    % de Y para este intervalo
    
    % Ex: 
    %     array de centroids:
    %     X     Y
    %     9    5
    %     10    6
    %     10    8
    %     10    2
    %     11    3
    %     12    7
    %     12    9
    %     13    2
    %     13    5
    %     14    3
    %     15    1
    %     15    3
    %     
    %     valor recebido em X: 12
    %     valor recebido em Y: 7
    %     intervalo definido : 1
    %
    %     então, procuramos o melhor valor de Y entre os valores que X+1 e
    %     X-1 (Entre 11 e 13)
    %
    %     11    3
    %     12    7 <-
    %     12    9
    %     13    2
    %     13    5
    %     
    %     Encontrou o melhor valor e retorna qual a posição dele na array
    %     para que seja removido
    


    best_value = Inf(1);
    best_pos = 0;

    interval_up     = base_value + interval;
    interval_bottom = base_value - interval;

    size_vector = length(centroids);

    for i=1:size_vector
        current_value = centroids(i,base_column);

        if current_value < interval_up && current_value > interval_bottom
           
            min = value - centroids(i,column);

            %Transforma em positivo
            min = sqrt(min^2);

            %Verifica se a distância é a menor obtida
            if min < best_value
                best_value = min;
                best_pos = i;
            end

        end
    end
        

end

