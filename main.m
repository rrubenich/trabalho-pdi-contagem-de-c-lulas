% Abre a tela de seleção de imagem
[filename, pathname, filterindex] = uigetfile({'*.jpg';'*.JPG';'*.PNG';'*.png'}, 'Selecione as imagens', 'MultiSelect', 'on');

% Matrix de contagem auxiliar
cells_matrix = zeros(0,5);

% Se só uma imagem for selecionada
if ischar(filename) == 1
    image_dir = strcat(pathname, filename);
    cells_matrix = cell_counter(image_dir, 1);
    
% Se mais de uma
else
    for im=1:length(filename)
        image_dir = strcat(pathname, filename{im});
        cells_matrix(im,:,:,:) = cell_counter(image_dir, im);
    end
end

%Salva o CSV
save_csv(cells_matrix)

close all;

