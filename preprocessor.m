function im = preprocessor( image )

    % Processa a imagem para encontrar as células
    
    % Funcionamento: 
    % binariza a imagem e faz a operação morfológica de erosão utilizando 
    % a forma disk

    seErode  = strel('disk', 4);
    
    erode = imerode(image, seErode);
    erode = im2bw(erode);
    im    = ~erode;

end

