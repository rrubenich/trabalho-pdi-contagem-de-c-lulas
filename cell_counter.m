function cells_matrix = cell_counter(image_dir, index)
    
    cells_matrix = zeros(0,5);
    
    % Abre a imagem que está na vez
    image     = imread(image_dir);

    % Faz o pré-processamento da imagem
    image_processed    = preprocessor(image);

    % Extrai os centroids a contagem
    [centroids, cells] = extract_cells(image_processed);

    % Variaveis de controle
    cells_removed = 0;
    cells_added = 0;

    % Mostra a imagem
    image_centroids = imshow(image_processed);

    hold on

    % Plota centroids
    plot(centroids(:,1),centroids(:,2), 'ro')

    while 1
        % Captura o click
        [x,y,button] = ginput(1);

        % Adiciona celula
        if button == 1

            centroids(end+1,1) = x;
            centroids(end,2) = y;
            plot(x, y, 'ro')
            cells_added = cells_added + 1;

        end

        % Remove celula
        if button == 3

            best_pos = best_min_interval(y, 2, x, 1, centroids, 10);

            if ~isnan(best_pos)
                centroids(best_pos,1) = 0;
                centroids(best_pos,2) = 0;
                imshow(image_processed)
                plot(centroids(:,1),centroids(:,2), 'ro')


                cells_removed = cells_removed + 1;
            end
        end

        % Para a edição
        if button == 2

            cells_matrix(1,1) = index;
            cells_matrix(1,2) = cells;
            cells_matrix(1,3) = cells_added;
            cells_matrix(1,4) = cells_removed;
            cells_matrix(1,5) = cells - cells_removed + cells_added;
            
            %Salva Imagem
            save_img(image_centroids,'processed',index);
            
            break
        end
    end

    hold off
end

